<?php require_once "./code.php"; ?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S1 A1</title>
</head>
<body>
	<fieldset>
		<h1>Full Address</h1>
		<h2>Places to Visit</h2>
		<p><strong>Ocean Park:</strong> <?= getFullAddress("Quirino Grandstand, 666 Behind", "Ermita", "Metro Manila", "Philippines"); ?></p>
		<p><strong>Star City:</strong> <?= getFullAddress("Roxas Blvd, CCP", "Pasay", "Metro Manila", "Philippines"); ?></p>
	
	</fieldset>
		
	<fieldset>
		<h1>Letter-Based Grading</h1>
		<p><?= getLetterGrade(100); ?></p>
		<p><?= getLetterGrade(97); ?></p>
		<p><?= getLetterGrade(94); ?></p>
		<p><?= getLetterGrade(91); ?></p>
		<p><?= getLetterGrade(88); ?></p>
		<p><?= getLetterGrade(85); ?></p>
		<p><?= getLetterGrade(82); ?></p>
		<p><?= getLetterGrade(79); ?></p>
		<p><?= getLetterGrade(76); ?></p>
		<p><?= getLetterGrade(74); ?></p>
		<p><?= getLetterGrade(105); ?></p>
	</fieldset>
</body>
</html>