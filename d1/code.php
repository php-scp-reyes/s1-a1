<?php //php tags - All PHP code must be written inbetween an opening and closing PHP tag ?>

<?php

//You can omit the closing php tag, but all your code from the opening and beyond will be in PHP

//[SECTION] Variables
//Used to contain data
//Named location for a stored value

$name = 'John Smith';
$email = 'johnsmith@gmail.com';

//Variable reassignment
//$name = 'Jane Doe';

//echo is used to print values
//echo $name;

//[SECTION] Constants
//Constants are used to hold data that is meant to be read-only and cannot be changed
define('PI', 3.1416);

//$PI = 200;

//echo $PI;

//echo PI;


// [SECTION] Data Types
// Strings
$state = 'New York';
$country = 'United States';

// Integers
$age = 31;
$headcount = 26;

// Floats
$grade = 98.2;
$distance = 1342.12;

// Boolean
$hasTraveledAbroad = false;
$hasSymptoms = true;

// Null
$spouse = null;
$middleName = null;

// Arrays
$grades = array(98.7, 92.1, 90.2, 94.6);
$students = ["John", "Jane", "Jacob"];

// Objects
$gradesObj =(object)[
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6
];

$personObj = (object)[
	'fullName' => 'John Smith',
	'isMarried' => false,
	'age' => 35,
	'address' => (object)[
		'state' => 'New York',
		'country' => 'United States'
	]
];

// [SECTION] Operators
// Assignments operator
// used to assign values to variables

$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;

//[SECTION] Functions
// 

function getFullName($firstName, $middleInitial, $lastName){
	return "$lastName, $firstName, $middleInitial";
}


// [SECTION] Selection Control Structures
// Selection Control Structures are used to make code execution dynamic according to predifined conditions

function determineTyphoonIntensity($windSpeed){
	if($windSpeed < 30){
		return "Not a typhoon yet";
	}else if($windSpeed <= 61){
		return "Tropical depression detected";
	}else if($windSpeed >=62 && $windSpeed <=88){
		return "Tropical storm detected";
	}else if($windSpeed >= 89 && $windSpeed <= 117){
		return "Severe tropical storm detected";
	}else{
		return 'Typhoon detected';
	}
}