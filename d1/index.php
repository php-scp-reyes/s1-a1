<?php require_once "./code.php";
//PHP code can be included from another file by using the require_once directive. ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S1: PHP Basics and Selection Control Structures</title>
</head>
<body>
	<h1>Echoing Values</h1>
	<p><?php echo 'Good day $name! Your given email is $email.'; ?></p>
	<?php //Double quotes can be used to print values with echo in the middle of any string ?>
	<p><?php echo "Good day $name! Your given email is $email."; ?></p>
	<?php //Concatenation (Dots are used instead of plus signs): ?>
	<p><?php echo 'Good day ' . $name . '!' . 'Your given email is ' . $email . '.'; ?></p>
	<p><?php echo PI; ?></p>
	<h1>Data Types</h1>
	<?php // Normal echoing of boolean and null variables is PHP will not show the values ?>
	<p><?php echo $hasTraveledAbroad; ?></p>
	<p><?php echo $spouse; ?></p>
	<?php// to see more detailed information on the variable, use var_dump ?>
	<p><?php echo var_dump($hasTraveledAbroad); ?></p>
	<?php // To see their types instead, we can use gettype() ?>
	<p><?php echo gettype($spouse); ?></p>
	<!-- to output an array   element, we can use-->
	<p><?php echo $grades[3]; ?></p>
	<p><?php echo $students[2]; ?></p>
	<!-- to output an entire array, we can use print_r() -->
	<pre><?php echo print_r($students); ?></pre>
	<!--  -->
	<p><?php echo $gradesObj->firstGrading; ?></p>
	<p><?php echo $personObj->address->state; ?></p>

	<h1>Operators</h1>
	<p><?= "X is $x"; ?></p>
	<p><?= "Y is $y"; ?></p>
	// <?php //"<?=" is shorthand for an opening PHP tag + echo ?>

	<h2>Arithmetic Operators</h2>
	<p>Sum: <?= $x + $y; ?></p>
	<p>Difference: <?= $x - $y; ?></p>
	<p>Product: <?= $x * $y; ?></p>
	<p>Quotient: <?= $x / $y; ?></p>

	<h2>Equality Operators</h2>
	<p>Loose Equality: <?= var_dump($x == '1342.14'); ?></p>
	<p>Strict Equality: <?= var_dump($x === '1342.14'); ?></p>
	<p>Loose Inequality: <?= var_dump($x != '1342.14'); ?></p>
	<p>Strict Inequality: <?= var_dump($x !== '1342.14'); ?></p>

	<h3>Greater/Lesser Operators</h3>
	<p>Is Lesser: <?= var_dump($x < $y); ?></p>
	<p>Is Greater: <?= var_dump($x > $y); ?></p>
	<p>Is Lesser or Equal: <?= var_dump($x <= $y); ?></p>
	<p>Is Greater or Equal: <?= var_dump($x >= $y); ?></p>

	<h2>Logical Operators</h2>
	<p>Is Legal Age: <?= var_dump($isLegalAge); ?></p>
	<p>Is Registered: <?= var_dump($isRegistered); ?></p>

	<p>Are All Requirements Met: <?= var_dump($isLegalAge && $isRegistered); ?></p>
	<p>Are Some Requirements Met: <?= var_dump($isLegalAge || $isRegistered); ?></p>
	<p>Are Some Requirements Not Met: <?= var_dump(!$isLegalAge || !$isRegistered); ?></p>

	<h1>Functions</h1>
	<p>Full Name: <?= getFullName("John", "B", "Smith"); ?></p>

	<h1>Selection Control Structures</h1>
	<p><?= determineTyphoonIntensity(135); ?></p>

</body>
</html>

<!-- JS -->
	<!-- array.push() -->

<!-- PHP -->
	<!-- push(value, array) -->